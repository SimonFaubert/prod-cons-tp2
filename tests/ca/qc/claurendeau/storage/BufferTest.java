package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class BufferTest {
	
	private static final int BUFFER_SIZE = 2;
	private Buffer buffer;
	private Element elem1 = Mockito.mock(Element.class);
	private Element elem2 = Mockito.mock(Element.class);
	private Element elem3 = Mockito.mock(Element.class);
	
	@Before
	public void setup() {
		buffer = new Buffer(BUFFER_SIZE);
		Mockito.when(elem2.getPrevious()).thenReturn(elem1);
	}
	
	@After
	public void teardown() {
		buffer = null;
	}
	
    @Test
    public void testBuildSuccess() {
        assertTrue(true);
    }
    
    @Test
    public void testBufferIsEmpty() {
    	assertTrue(buffer.isEmpty());
    }
    
    @Test
    public void testBufferNotEmpty() throws BufferFullException {
    	buffer.addElement(elem1);
    	assertFalse(buffer.isEmpty());
    }
    
    @Test
    public void testCapacity() {
    	assertEquals(BUFFER_SIZE, buffer.capacity());
    }
    
    @Test
    public void testIsNotFull() {
    	assertFalse(buffer.isFull());
    }
    
    @Test
    public void testIsFull() throws BufferFullException {
    	buffer.addElement(elem1);
    	buffer.addElement(elem2);
    	assertTrue(buffer.isFull());
    }
    
    @Test
    public void testGetCurrentLoad() throws BufferFullException {
    	assertEquals(0, buffer.getCurrentLoad());
    	buffer.addElement(elem1);
    	assertEquals(1, buffer.getCurrentLoad());
    }
    
    @Test
    public void testaddOneElement() throws BufferFullException, BufferEmptyException {
    	buffer.addElement(elem1);
    	assertFalse(buffer.isEmpty());
    	assertEquals(elem1, buffer.removeElement());
    	Mockito.verify(elem1, Mockito.times(1)).setPrevious(null);
    }
    
    @Test
    public void testAddTwoElements() throws BufferFullException, BufferEmptyException {
    	buffer.addElement(elem1);
    	buffer.addElement(elem2);
    	assertEquals(2, buffer.getCurrentLoad());
    	assertEquals(elem2, buffer.removeElement());
    	assertEquals(elem1, buffer.removeElement());
    	Mockito.verify(elem1, Mockito.times(1)).setPrevious(null);
    	Mockito.verify(elem2, Mockito.times(1)).setPrevious(elem1);
    }

    @Test
    public void testRemoveOne() throws BufferFullException, BufferEmptyException {
    	buffer.addElement(elem1);
    	assertEquals(elem1, buffer.removeElement());
    	assertTrue(buffer.isEmpty());
    }
    
    @Test
    public void testRemoveTwoElements() throws BufferFullException, BufferEmptyException {
    	buffer.addElement(elem1);
    	buffer.addElement(elem2);
    	assertEquals(elem2, buffer.removeElement());
    	assertEquals(elem1, buffer.removeElement());
    	assertTrue(buffer.isEmpty());
    }
    
    @Test (expected = BufferFullException.class)
    public void testAddTooMany() throws BufferFullException {
    		buffer.addElement(elem1);
			buffer.addElement(elem2);
			buffer.addElement(elem3);
    }
    
    @Test (expected = BufferEmptyException.class)
    public void testRemoveWhenEmpty() throws BufferEmptyException {
    	buffer.removeElement();
    }
    
    @Test
    public void testToString() throws BufferFullException {
    	Mockito.when(elem1.getData()).thenReturn(1);
    	Mockito.when(elem2.getData()).thenReturn(2);
    	buffer.addElement(elem1);
    	buffer.addElement(elem2);
    	assertEquals("Buffer1,2", buffer.toString());
    }
}


