package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
    
	private static final long serialVersionUID = -5244712183993434093L;

	public BufferFullException() {
		super("Attempted to insert into Buffer when it is full.");
	}
}
