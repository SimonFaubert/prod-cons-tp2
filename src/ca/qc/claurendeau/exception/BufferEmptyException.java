package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {

	private static final long serialVersionUID = -5817551533855897656L;

	public BufferEmptyException() {
		super("Attempted to remove an element from empty Buffer");
	}
}
