package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
    private int nbElement;
    private int capacity;
    private Element element;
    
    public Buffer(int capacity)
    {
    	this.capacity = capacity;
    	nbElement = 0;
    }

    @Override
	public String toString() {
		String elements = "";
    	Element currentElement = element;
    	while(currentElement != null) {
    		elements = currentElement.getData() + elements;
    		if (currentElement.getPrevious() != null) {
				elements = "," + elements;
			}
    		currentElement = currentElement.getPrevious();
    	}
    	return "Buffer" + elements+"";
	}

    // returns the capacity of the buffer
    public int capacity()
    {
        return capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
       return nbElement;
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        return nbElement == 0;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        return capacity<=nbElement;
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
    	if(isFull()) {
    		throw new BufferFullException();
    	}
    	element.setPrevious(this.element);
    	this.element = element;
    	nbElement ++;
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement() throws BufferEmptyException
    {
    	if (isEmpty()) {
			throw new BufferEmptyException();
		}
    	Element toReturn = element;
    	this.element = element.getPrevious();
        nbElement--;
    	return toReturn;
    }
}
