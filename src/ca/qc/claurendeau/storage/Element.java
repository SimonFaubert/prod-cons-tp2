package ca.qc.claurendeau.storage;

public class Element
{
    Element previous;
    int data;

	public void setData(int data)
    {
        this.data = data;
    }
    
    public int getData()
    {
        return data;
    }
    
    public Element getPrevious() {
		return previous;
	}

	public void setPrevious(Element previous) {
		this.previous = previous;
	}
}
